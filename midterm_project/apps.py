from django.apps import AppConfig


class MidtermProjectConfig(AppConfig):
    name = 'midterm_project'
